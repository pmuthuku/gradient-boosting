#!/bin/bash
###########################################################################
##                                                                       ##
##                     Carnegie Mellon University                        ##
##                         Copyright (c) 2015                            ##
##                        All Rights Reserved.                           ##
##                                                                       ##
##  Permission is hereby granted, free of charge, to use and distribute  ##
##  this software and its documentation without restriction, including   ##
##  without limitation the rights to use, copy, modify, merge, publish,  ##
##  distribute, sublicense, and/or sell copies of this work, and to      ##
##  permit persons to whom this work is furnished to do so, subject to   ##
##  the following conditions:                                            ##
##   1. The code must retain the above copyright notice, this list of    ##
##      conditions and the following disclaimer.                         ##
##   2. Any modifications must be clearly marked as such.                ##
##   3. Original authors' names are not deleted.                         ##
##   4. The authors' names are not used to endorse or promote products   ##
##      derived from this software without specific prior written        ##
##      permission.                                                      ##
##                                                                       ##
##  CARNEGIE MELLON UNIVERSITY AND THE CONTRIBUTORS TO THIS WORK         ##
##  DISCLAIM ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING      ##
##  ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT   ##
##  SHALL CARNEGIE MELLON UNIVERSITY NOR THE CONTRIBUTORS BE LIABLE      ##
##  FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES    ##
##  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN   ##
##  AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,          ##
##  ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF       ##
##  THIS SOFTWARE.                                                       ##
##                                                                       ##
###########################################################################
##                                                                       ##
##       Author: Prasanna Kumar (pmuthuku@cs.cmu.edu) Jan 2015           ##
##                                                                       ##
###########################################################################
##                                                                       ##
##  An attempt at using Gradient boosted trees rather than Random        ##
##  Forests.                                                             ##
##                                                                       ##
###########################################################################

num_trees=10

cd festival
mkdir -p gb_trees

# We will try to maintain a directory structure similar to the one for the
# random forest just so that it is easier to run experiments
if [ ! -d ../rf_models ]
then
    ln -s `pwd`/gb_trees ../rf_models
fi

cp clunits/mcep.desc gb_trees/mcep.desc.base
SEED=`echo $$ | awk '{ print $1%10000 }'`
echo $SEED > gb_trees/seed

for i in `seq 1 $num_trees`
do
    j=`printf "%02d" $i`
    mkdir -p gb_trees/trees_$j

    SSEED=`cat gb_trees/seed`
    # make random description file
    # Keep first two features always
    cat gb_trees/mcep.desc.base |
    awk 'BEGIN {for (i=1;i<'$SSEED'; i++)
                      randint(10)}
        function randint(n) { return int(n * rand()) }
        {if (NR < 3)
         {
            print $0
         }
         else if (substr($0,1,1) == "(")
         {
            p = randint(100);
            if (p < 40)
            {
               printf("%s %s ignore ",$1,$2);
               for (i=3; i<=NF; i++)
                  printf("%s ",$i);
               printf("\n");
            }
            else
               print $0
         }
         else
            print $0
        }' > gb_trees/trees_$j/mcep.desc

   echo $SSEED |
   awk 'function randint(n) { return int(n * rand()) }
        {for (i=1;i<$1; i++)
                 n=randint(10000);
         print n}' > gb_trees/seed
    
done


# The first set of trees are the ones that the default build creates.
# So, let's copy them over to first tree directory
cp -p trees/*_?_mcep.tree gb_trees/trees_01/
cp -p trees/cmu_us_* gb_trees/trees_01/

mv trees first_trees

# Build boosted trees
# for trees_02

# script trees_02


