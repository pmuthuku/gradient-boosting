#!/bin/bash
###########################################################################
##                                                                       ##
##                     Carnegie Mellon University                        ##
##                         Copyright (c) 2015                            ##
##                        All Rights Reserved.                           ##
##                                                                       ##
##  Permission is hereby granted, free of charge, to use and distribute  ##
##  this software and its documentation without restriction, including   ##
##  without limitation the rights to use, copy, modify, merge, publish,  ##
##  distribute, sublicense, and/or sell copies of this work, and to      ##
##  permit persons to whom this work is furnished to do so, subject to   ##
##  the following conditions:                                            ##
##   1. The code must retain the above copyright notice, this list of    ##
##      conditions and the following disclaimer.                         ##
##   2. Any modifications must be clearly marked as such.                ##
##   3. Original authors' names are not deleted.                         ##
##   4. The authors' names are not used to endorse or promote products   ##
##      derived from this software without specific prior written        ##
##      permission.                                                      ##
##                                                                       ##
##  CARNEGIE MELLON UNIVERSITY AND THE CONTRIBUTORS TO THIS WORK         ##
##  DISCLAIM ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING      ##
##  ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT   ##
##  SHALL CARNEGIE MELLON UNIVERSITY NOR THE CONTRIBUTORS BE LIABLE      ##
##  FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES    ##
##  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN   ##
##  AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,          ##
##  ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF       ##
##  THIS SOFTWARE.                                                       ##
##                                                                       ##
###########################################################################
##                                                                       ##
##       Author: Prasanna Kumar (pmuthuku@cs.cmu.edu) Jan 2015           ##
##                                                                       ##
###########################################################################
##                                                                       ##
##  Given a particular tree set and a statename, make predictions for    ##
##  the training data.                                                   ##
##                                                                       ##
###########################################################################

tree_nm=$1
state_nm=`basename $2 _mcep.tree`


# Some bash tricks to get the name of the previous tree set
tmp1=`echo "${tree_nm:(-2)} - 1" | bc`
prev_tree=trees_`printf "%02d" $tmp1`


CG_TMP=jnk_$$

echo "Getting the MCEP predictions of $state_nm in $tree_nm"

$ESTDIR/bin/wagon_test -tree $2 -desc clunits/mcep.desc -data feats/$state_nm.feats -predict -o $CG_TMP.wagon_preds

# Get rid of Standard Deviations (or are they variances?)
sed 's/[()]/ /g' $CG_TMP.wagon_preds | perl -ane '
    for ($i=0; $i <= ($#F-1); $i=$i+2){
         print "$F[$i] ";
    }
    print "\n";'| $SPTKDIR/bin/x2x +af > gb_trees/$tree_nm/$state_nm.preds

# Get combined prediction of every tree built so far
if [ $tree_nm == "trees_01" ]
then
    # There are no previous trees. So, don't need to combine anything
    # cp gb_trees/$tree_nm/$state_nm.preds gb_trees/$tree_nm/$state_nm.sum_preds
    cp gb_trees/$tree_nm/$state_nm.preds gb_trees/$tree_nm/$state_nm.comb_preds
else
    # Combine the prediction of previous trees
    # The combined prediction is the prediction of the current tree plus the prediction
    # of all previous trees
    $SPTKDIR/bin/vopr -a gb_trees/$tree_nm/$state_nm.preds gb_trees/$prev_tree/$state_nm.comb_preds > gb_trees/$tree_nm/$state_nm.comb_preds
fi

# Compute residual (gradient) between the combined predictions and truth
$ESTDIR/bin/ch_track disttabs/$state_nm.mcep | $SPTKDIR/bin/x2x +af > $CG_TMP.true
$SPTKDIR/bin/vopr -s  $CG_TMP.true gb_trees/$tree_nm/$state_nm.comb_preds |
$SPTKDIR/bin/x2x +fa52 | #Need to generalize this
$ESTDIR/bin/ch_track -itype ascii -otype est_ascii -s 0.005 -o gb_trees/$tree_nm/$state_nm.res



rm -f $CG_TMP.*
