Instructions for running my Gradient Boosting scripts:

Notes: These were some hacky scripts that I wrote over a winter break.
The boosted trees kept overfitting to the training data, and so the 
results were never better than using a single tree, let alone the 
random forest. I'm just documenting the code here so that some future
brilliant graduate student may find some use for it.

i) Run the standard single tree build script.

ii) In the voice directory, run wagon_gradient_boost.sh. This script
sets up the directories where the boosting code will put its trees.

iii) In the voice/festival/ directory, run build_boosted_trees:
./build_boosted_trees.sh trees_xx FV_VOICENAME

Here's an example:
 ~/code/Gradient_boosting/build_boosted_trees.sh trees_02 cmu_us_slt

You can do this for as many trees as you have setup in the 
wagon_gradient_boost.sh script. The default is set to 10.

iv) Once you have done this, in clustergen.scm,
(set! cg:rfs 10)
or some other appropriate number.

In the same file, look for the function, ClusterGen_predict_mcep.
In this function, keep searching for rfs_info. You will find a line
with this: (length rfs_info). Replace this with 1. (In Gradient 
boosting, the combined prediction of the trees is the sum, not the 
average in random forests)

v) In rf_models, create a file called mlist listing the trees that
you want the system to use. Example contents of mlist:
01 02 03

vi) cg_test should do the right thing.

P.S. These scripts aren't very robust. You have to set some symbolic
links appropriately, and move the mcep.desc file back and forth. But
since Alan is probably the only one who will try to run these scripts,
I'm going to leave them as is :).
